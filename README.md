#  Product Catalog MS v1 

This is a Microservice build in SpringBoot to make CRUD operations .



### See More 
##### [SpringBoot](https://spring.io/projects/spring-boot)
##### [Maven](https://maven.apache.org/)
##### [H2 Database](https://www.h2database.com/html/main.html)
##### [Orika](https://orika-mapper.github.io/orika-docs/)