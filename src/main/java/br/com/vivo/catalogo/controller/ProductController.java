package br.com.vivo.catalogo.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.vivo.catalogo.config.mapper.ProductInMapper;
import br.com.vivo.catalogo.config.mapper.ProductOutMapper;
import br.com.vivo.catalogo.domain.Product;
import br.com.vivo.catalogo.dto.ProductDTORequest;
import br.com.vivo.catalogo.dto.ProductDTOResponse;
import br.com.vivo.catalogo.service.ProductService;
import br.com.vivo.catalogo.util.Util;

/***
 * 
 * @author A0098672
 *
 *         This class controller spring is responsible for Operations with
 *         products
 */
@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private  ProductService productService;

	@Autowired
	private  ProductInMapper productMapperInFacade;

	@Autowired
	private  ProductOutMapper productMapperOutFacade;

	// LOGGER
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// GET ALL
	/**
	 * Get All Products
	 * 
	 * @param uriBuilder 
	 * @return List<Product> 
	 */
	@GetMapping
	public List<ProductDTOResponse> getAllProducts(UriComponentsBuilder uriBuilder) {
		
		logger.info(" getAllProducts : REQUEST  [  GET_ALL  ] ");
		
		List<ProductDTOResponse> listDtoResponse = productMapperOutFacade.mapAsList(productService.getAll(), ProductDTOResponse.class);
		
		logger.info("getAllProducts : RESPONSE  [   {}   ] ", listDtoResponse);
		
		return listDtoResponse;
	}

	// GET BY ID
	/**
	 * Get Product By Id
	 * @param id
	 * @param uriBuilder
	 * @return Product
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ProductDTOResponse> getProductById(@PathVariable Long id, UriComponentsBuilder uriBuilder) {
		logger.info("getProductById  :  REQUEST  [  id  :  {}  ] ", id);
		
		ProductDTOResponse response = productMapperOutFacade.map(productService.getById(id), ProductDTOResponse.class); 
		
		if(response == null) {
			return ResponseEntity.notFound().build();
		}
		
		
		logger.info(" getProductById : RESPONSE  [   {}   ] ", response );
	
		return ResponseEntity.ok(response);
	}

	// GET BY Search
	/**
	 * 
	 * Get a list of filtered Products 
	 * 
	 * @param minPrice
	 * @param maxPrice
	 * @param q
	 * @param uriBuilder
	 * @return List<Product> 
	 */
	@GetMapping("/search")
	public List<ProductDTOResponse> getProductBySearch(@RequestParam(required = false , name = "min_price") String minPrice,
			@RequestParam(required = false , name = "max_price") String maxPrice, @RequestParam(required = false) String q,
			UriComponentsBuilder uriBuilder) {

		
		//AJUSTE : SE ELE N�O FILTRO NENHUM , NEM FAZ A BUSCA 
		List<ProductDTOResponse> listDtoResponse = new ArrayList<ProductDTOResponse>();
		if(q== null && minPrice == null && maxPrice == null) {
			return listDtoResponse ;
		}
		
		
		double minPriceCurrent = Util.getRangedMinNumericValueFilter(minPrice);
		double maxPriceCurrent = Util.getRangedMaxNumericValueFilter(maxPrice);
				
		logger.info("getProductBySearch : REQUEST  [  q  :  {} , max_prixe : {} , min_price : {}  ]", q , maxPriceCurrent , minPriceCurrent);

		listDtoResponse = productMapperOutFacade.mapAsList(productService.getBySearch(minPriceCurrent, maxPriceCurrent, q), ProductDTOResponse.class);
		
		logger.info("getProductBySearch : RESPONSE  [   {}   ] ", listDtoResponse);

		return listDtoResponse;

	}

	// CREATE PRODUCT
	/**
	 * 
	 * Create a Product
	 * 
	 * @param productDto
	 * @param uriBuilder
	 * @return Product
	 */
	@PostMapping
	public ResponseEntity<ProductDTOResponse> createProduct(@Valid @RequestBody ProductDTORequest productDto,
			UriComponentsBuilder uriBuilder) {
		
		logger.info("createProduct : REQUEST  [ {} ]", productDto);
		
		ProductDTOResponse response = productMapperOutFacade.map(
				productService.create(productMapperInFacade.map(productDto, Product.class)), ProductDTOResponse.class);
		
		logger.info("createProduct : RESPONSE  [ {} ]", productDto);
		URI uri = uriBuilder.path("/products/{id}").buildAndExpand(response.getId()).toUri();
		return ResponseEntity.created(uri).body(response);
	}

	// UPDATE PRODUCT
	/**
	 * Update a Product
	 * @param id
	 * @param productDto
	 * @param uriBuilder
	 * @return Product
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ProductDTOResponse> updateProduct(@PathVariable Long id,
			@RequestBody @Valid ProductDTORequest productDto, UriComponentsBuilder uriBuilder) {
		
		logger.info("updateProduct : REQUEST  [ {} ]", productDto);
		ProductDTOResponse response = productMapperOutFacade.map(
				productService.update(id, productMapperInFacade.map(productDto, Product.class)),
				ProductDTOResponse.class);

		
		logger.info("updateProduct : RESPONSE  [ {} ]", productDto);
		return ResponseEntity.ok(response);
	}

	// DELETE PRODUCT
	/**
	 * Delete Product 
	 * @param id
	 * @param uriBuilder
	 * @return true if deleted false if not 
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<ProductDTOResponse> deleteProduct(@PathVariable Long id, UriComponentsBuilder uriBuilder) {
		
		logger.info("deleteProduct : REQUEST  [  id  :  {}  ] ", id);
		boolean deleted = productService.delete(id);

		if (!deleted)
			return ResponseEntity.notFound().build();

		logger.info("deleteProduct : RESPONSE   [  DELETED  :  {}  ] ", deleted);
		return ResponseEntity.ok().build();

	}

}
