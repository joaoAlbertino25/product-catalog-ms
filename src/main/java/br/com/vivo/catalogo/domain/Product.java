package br.com.vivo.catalogo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

/***
 * This class is resposible to represent the domain product
 * 
 * @author Joao Albertino
 * @version 1.0
 *  
 */
@Entity
@Data
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String description;

	private double price;

	@Override
	public String toString() {
		return "Produto [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + "]";
	}

}
