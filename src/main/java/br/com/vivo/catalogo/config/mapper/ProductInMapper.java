package br.com.vivo.catalogo.config.mapper;

import org.springframework.stereotype.Component;

import br.com.vivo.catalogo.domain.Product;
import br.com.vivo.catalogo.dto.ProductDTORequest;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
public class ProductInMapper extends ConfigurableMapper {

	@Override
	protected void configure(MapperFactory factory) {
		factory.classMap(ProductDTORequest.class, Product.class).byDefault().register();
	}

}
