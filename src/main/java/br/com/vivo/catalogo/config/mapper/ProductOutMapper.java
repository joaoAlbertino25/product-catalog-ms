package br.com.vivo.catalogo.config.mapper;

import org.springframework.stereotype.Component;

import br.com.vivo.catalogo.domain.Product;
import br.com.vivo.catalogo.dto.ProductDTOResponse;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

@Component
public class ProductOutMapper extends ConfigurableMapper  {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Product.class , ProductDTOResponse.class).byDefault().register();
    }

}
