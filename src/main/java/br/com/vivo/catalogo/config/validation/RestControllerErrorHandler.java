package br.com.vivo.catalogo.config.validation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.vivo.catalogo.dto.ErrorHandlerDTO;

@RestControllerAdvice
public class RestControllerErrorHandler {

	@Autowired
	private MessageSource messageSource;

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ErrorHandlerDTO handlerBadRequest(MethodArgumentNotValidException exception) {
		List<FieldError> errors = exception.getBindingResult().getFieldErrors();
		StringBuilder errorMessages = new StringBuilder(); 
		int counter = errors.size(); 
		for(FieldError err : errors ){
			
			errorMessages.append("["+err.getField()+"] "+messageSource.getMessage(err, LocaleContextHolder.getLocale()));
			
			if(counter > 1 ) {
				errorMessages.append(". ");
			}
			
			counter --;
		};
		
		ErrorHandlerDTO errDto = new ErrorHandlerDTO();
		errDto.setStatus_code(HttpStatus.BAD_REQUEST.value());
		errDto.setMessage(errorMessages.toString());

		return errDto;
	}

	
}
