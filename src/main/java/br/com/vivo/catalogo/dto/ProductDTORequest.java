package br.com.vivo.catalogo.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

import org.springframework.lang.NonNull;

import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * This Class Represent Product DTO Request 
 * 
 * @author Joao Albertino
 * @version 1.0
 */
@Data
@NoArgsConstructor
public class ProductDTORequest {
	
	@NonNull @NotEmpty
	private String name;
	
	@NonNull @NotEmpty
	private String description;
	
	@NonNull @NotEmpty @Positive
	private String price;


	@Override
	public String toString() {
		return "Produto [name=" + name + ", description=" + description + ", price=" + price + "]";
	}

	

}
