package br.com.vivo.catalogo.dto;

import lombok.Data;

/***
 * This Class Represent Product DTO Response 
 * 
 * @author Joao Albertino
 * @version 1.0
 */
@Data
public class ProductDTOResponse {
	
	private Long id ; 
	private String name ; 
	private String description ; 
	private String price ;
	
	@Override
	public String toString() {
		return "Produto [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + "]";
	} 
	
}
