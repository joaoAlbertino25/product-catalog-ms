package br.com.vivo.catalogo.dto;

import lombok.Data;


/***
 * This Class Represent Error DTO Response 
 * 
 * @author Joao Albertino
 * @version 1.0
 */
@Data
public class ErrorHandlerDTO {

	public  int status_code;
    private String message;
	
}
