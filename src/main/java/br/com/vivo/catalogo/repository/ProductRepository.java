package br.com.vivo.catalogo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.vivo.catalogo.domain.Product;


public interface ProductRepository extends JpaRepository<Product, Long> {
	
	
	/**
	 * 
	 * Get Products Filtered By minValue , maxValue , description and  name
	 * 
	 * @param minPrice
	 * @param maxPrice
	 * @param text
	 * 
	 * @return List of Products
	 */
	@Query("SELECT p FROM Product p WHERE (  p.price >= ?1 AND p.price <= ?2 )  AND ( ?3 is  null OR lower(p.name) like lower(CONCAT('%',?3,'%'))   OR lower(p.description) like lower(CONCAT('%',?3,'%'))  )")
	//@Query("SELECT p FROM Product p WHERE (  p.price >= ?1 AND p.price <= ?2 )  AND ( ?3 is null OR lower(p.name) like ?3   OR lower(p.description) like ?3  )")
	public List<Product> getProductsByPriceOffsetNameDescription(double minPrice , double maxPrice, String text);

}
