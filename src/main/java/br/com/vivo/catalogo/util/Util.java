package br.com.vivo.catalogo.util;


/***
 * This Class Represent Util class with generic functions 
 * 
 * @author Joao Albertino
 * @version 1.0
 */
public class Util {
	
	
	/**
	 * Get Max Value of Double Value Price .
	 * 
	 * If the maxValue is Null returns the Double max value
	 * 
	 * @param maxValue
	 * @return Double maxValue
	 */
	public static double getRangedMaxNumericValueFilter(String maxValue ) {
		return maxValue == null ? Double.MAX_VALUE : Double.parseDouble(maxValue);
	}
	
	
	/**
	 * Get Min Value of Double Value Price .
	 * 
	 * If the minValue is Null returns the Double min value
	 * 
	 * @param maxValue
	 * @return Double minValue
	 */
	public static double getRangedMinNumericValueFilter(String minValue ) {
		return minValue == null ? Double.MIN_VALUE : Double.parseDouble(minValue);
	}

}
