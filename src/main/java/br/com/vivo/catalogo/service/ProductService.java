package br.com.vivo.catalogo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vivo.catalogo.domain.Product;
import br.com.vivo.catalogo.repository.ProductRepository;


/***
 * This Class Represent Product Service
 * 
 * @author Joao Albertino
 * @version 1.0
 */
@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	
	/**
	 * Get all Products 
	 * @return List all of Products 
	 */
	public List<Product> getAll() {
		return productRepository.findAll();
	}
	
	
	/**
	 * Get Product By Id
	 * @param id
	 * @return Product 
	 */
	public Product getById(Long id) {
		Optional<Product> currentProduct = productRepository.findById(id);

		if (!currentProduct.isPresent()) {
			return null;
		}
		return currentProduct.get();
	}

	/***
	 * Get All Products Filtered 
	 * 
	 * @param minPrice
	 * @param maxPrice
	 * @param text
	 * 
	 * @return List of Products
	 */
	public List<Product> getBySearch(double minPrice , double maxPrice , String text) {
		return productRepository.getProductsByPriceOffsetNameDescription(minPrice , maxPrice , text);
	}

	
	/***
	 * Create a Product
	 * 
	 * @param product
	 * 
	 * @return Product
	 */
	@Transactional
	public Product create(Product product) {
		productRepository.save(product);
		return product;
	}

	
	/***
	 * Update a Product
	 * 
	 * @param id
	 * @param product
	 * 
	 * @return Product
	 */
	@Transactional
	public Product update(Long id, Product product) {
		Optional<Product> currentProduct = productRepository.findById(id);

		if (!currentProduct.isPresent()) {
			return null;
		}

		currentProduct.get().setDescription(product.getDescription());
		currentProduct.get().setName(product.getName());
		currentProduct.get().setPrice(product.getPrice());

		productRepository.save(currentProduct.get());
		
		return currentProduct.get();
	}

	/***
	 * Delete a product 
	 * 
	 * @param id
	 * 
	 * @return true if deleted or false if not
	 */
	@Transactional
	public boolean delete(Long id) {
		Optional<Product> currentProduct = productRepository.findById(id);

		if (!currentProduct.isPresent()) {
			return false;
		}
		
		productRepository.deleteById(id);
		return true;
	}

}
